#' Spark box pour les chiffres clefs
#'
#' @param data le df avec les séries
#' @param indic Autorisations ou Mises en chantier
#' @param zone la Zone sur laquelle afficher la série
#'
#' @return un htmlWidget
#' @export
#'
#' @examples
#' library(dplyr)
#' data <- filter_data_input("Pays de la Loire")
#' sitadel_spark_box(
#'   data = data$df,
#'   indic = "Mises en chantier",
#'   zone = data$reg
#' )
#' @importFrom apexcharter spark_box
#' @importFrom dplyr filter select slice_tail last

sitadel_spark_box <- function(data, indic,zone) {
  st <- ifelse(indic == "Autorisations", "Logements autoris\u00e9s", "Logements mis en chantier")
  tt <- data %>%
    dplyr::filter(.data$indicateur == indic, .data$TypeLogement == "Logements", .data$Zone==zone) %>%
    dplyr::select(date, `Nombre de logements` = cumul12) %>% 
    dplyr::slice_tail(n = 60)
  apexcharter::spark_box(
    data = tt,
    title = dplyr::last(tt$`Nombre de logements`),
    subtitle = st
  )
}
